#!/usr/bin/python
# -*- coding: utf-8 -*-

# cargamos la libreria con un alias
from mysmb import MYSMB
from impacket import smb, smbconnection, nt_errors
from impacket.uuid import uuidtup_to_bin
from impacket.dcerpc.v5.rpcrt import DCERPCException
from struct import pack
import time
import nmap
import threading
import random
import os
import sys
import pyTemplate as pytemp


# devuelve True si el puerto 445 esta abierto
def check_port445(ip):
    res = False
    nm = nmap.PortScanner()
    r = nm.scan(ip, arguments="-Pn -p 445")
    estado = r['scan'][ip]['tcp'][445]['state']
    if estado == "open":
        res = True
    return res


# devuelve True si se puede loguear
def check_login(ip):
    USERNAME = ''
    PASSWORD = ''
    res = False
    try:
        conn = MYSMB(ip)
    	conn.login(USERNAME, PASSWORD)
        res = True
    except:
    	return False
    return res


# dice si una ip es vulnerable o no
def check_ip(ip):
    USERNAME = ''
    PASSWORD = ''

    NDR64Syntax = ('71710533-BEBA-4937-8319-B5DBEF9CCC36', '1.0')

    MSRPC_UUID_BROWSER  = uuidtup_to_bin(('6BFFD098-A112-3610-9833-012892020162','0.0'))
    MSRPC_UUID_SPOOLSS  = uuidtup_to_bin(('12345678-1234-ABCD-EF00-0123456789AB','1.0'))
    MSRPC_UUID_NETLOGON = uuidtup_to_bin(('12345678-1234-ABCD-EF00-01234567CFFB','1.0'))
    MSRPC_UUID_LSARPC   = uuidtup_to_bin(('12345778-1234-ABCD-EF00-0123456789AB','0.0'))
    MSRPC_UUID_SAMR     = uuidtup_to_bin(('12345778-1234-ABCD-EF00-0123456789AC','1.0'))

    pipes = {
        'browser'  : MSRPC_UUID_BROWSER,
        'spoolss'  : MSRPC_UUID_SPOOLSS,
        'netlogon' : MSRPC_UUID_NETLOGON,
        'lsarpc'   : MSRPC_UUID_LSARPC,
        'samr'     : MSRPC_UUID_SAMR,
    }

    conn = MYSMB(ip)
    try:
        conn.login(USERNAME, PASSWORD)
    except:
        pass
    finally:
        os = conn.get_server_os()

    tid = conn.tree_connect_andx('\\\\'+ip+'\\'+'IPC$')
    conn.set_default_tid(tid)

    # test if target is vulnerable
    TRANS_PEEK_NMPIPE = 0x23
    recvPkt = conn.send_trans(pack('<H', TRANS_PEEK_NMPIPE), maxParameterCount=0xffff, maxDataCount=0x800)
    status = recvPkt.getNTStatus()
    if status == 0xC0000205:
        print "%s: VULNERABLE (%s)" % (ip, os)
    else:
        print "%s: Parcheado (%s)" % (ip, os)


# devuelve True si la IP es rutable
def ipIsUp(ip):
    result = os.system("ping -c 1 -W 40 %s > /dev/null" % ip)
    if result == 0:
        return True
    else:
        return False


# genera una lista aleatoria
def genera_lista():
    nm = nmap.PortScanner()
    r = nm.scan(arguments='-iR 1000 -sn')
    f = open("ips.txt", "a")
    for host in r['scan']:
        if check_port445(host):
            f.write(host + "\n")
    f.close()


def check_one_ip(ip):
    ip = ip.replace("\n", "")
    # if ipIsUp(ip):
    if check_port445(ip):
        if check_login(ip):
            check_ip(ip)
        else:
            print ip + ": Error al loguear"
    else:
        print ip + ": Puerto 445 cerrado"
    # else:
    #    print ip + ": Esta IP no responde"


if __name__ == '__main__':
    # creamos un objeto de tipo aplicación, con los datos de nuestro script
    app = pytemp.Aplicacion(
        "pyEternalChecker",
        "Genera/Comprueba lista de IPs vulnerables para EternalBlue",
        "https://gitlab.com/srmojon/pyEternalChecker",
        "dependencias",
        )
    app.add_op_valor("--ip", "Especifica una IP como parametro.")
    app.add_op_valor("--lista", "Especifica una lista de IPs como parametro.")
    app.add_op_valor("--rango", "Escanea un rango ('random' para generar uno aleatorio)")
    app.add_op_valor("--genlist", "Genera una lista aleatoria de IPs (requiere numero de threads).", int)

    # checkeamos los parametros (obligatorio) y mostramos el array que nos devuelve
    args = app.check_args()

    # analiza una ip
    if args.ip:
        check_one_ip(args.ip)

    # analiza una lista de ips
    if args.lista:
        f = open(args.lista, 'r')
        for ip in f:
            check_one_ip(ip)
        f.close()

    # genera una lista de ips
    if args.rango:
        start_time = time.time()
        nm = nmap.PortScanner()
        # genera rango /24 aleatorio
        if args.genlist == "random":
            while True:
                a = random.randint(1, 254)
                b = random.randint(1, 254)
                c = random.randint(1, 254)
                rango = "%s.%s.%s.0/24" %(a, b, c)
                print "Escaneando en el rango: " + rango
                r = nm.scan(hosts=rango, ports="445", arguments='--open')
                if len(nm.all_hosts()) > 0:
                    break
                print "No se han encontrado IPs con el puerto 445."
                print "Volviendo a escanear en 5s..."
                time.sleep(5)
        # escanea rango asignado por el usuario
        else:
            r = nm.scan(hosts=args.rango, arguments='-Pn -p 445 --open')

        # muestra resultados
        resultados = r['scan']
        elapsed_time = time.time() - start_time
        print "\n * Duración del script: %.8f segundos." % elapsed_time
        print " IPs encontradas: " + str(len(resultados))
        for ip in resultados:
            check_one_ip(ip)

    # crea varios threads para buscar puertos 445 abiertos
    if args.genlist:
        start_time = time.time()
        numero_threads = args.genlist
        print " Generando lista aleatoria..."
        threads = list()
        for i in range(numero_threads):
            t = threading.Thread(target=genera_lista)
            threads.append(t)
            t.start()
            print "  Thread %s iniciado" % str(i+1)
            time.sleep(1)

        while True:
            app.limpia_pantalla()
            suma = 0
            for i in threads:
                if i.isAlive():
                    suma += 1
            if suma == 0:
                break
            print " Treads en marcha: %s \n" % str(len(threads))
            pytemp.cuenta_atras("  Comprobando de nuevo en ", 10)
            time.sleep(1)

        print " IPs encontradas: " + str(pytemp.cuenta_lineas("ips.txt"))
        elapsed_time = time.time() - start_time
        print "\n * Duración del script: %.8f segundos." % elapsed_time
        sys.exit("Terminado")
