#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import math

# intena cargar las dependencias de pyTemplate
try:
    import argparse
    import time
# si no se pueden cargar, lo notifica y sale
except:
    print "\n\tFaltan dependencias."
    print "\tDependencias necesarias: argparse, time \n"
    sys.exit()


# clase basica de script
class Aplicacion:
    def __init__(
        self, nombre="Nombre del script/aplicación",
        descripcion="Descripción del script/aplicación",
        enlace="https://gitlab.com/srmojon/pyTemplate",
        dependencias="listado de dependencias", debug=True
    ):
        """Requiere 4 parametros de tipo string (configurables):
        :nombre str
            Nombre del scrip/aplicación (TODO: test con espacios y simbolos)
        :descripción str
            Descripción breve que se me muestra en la cabecera."""
        self.nombre = str(nombre)
        self.descripcion = str(descripcion)
        self.enlace = str(enlace)
        self.deps = str(dependencias)
        self.ruta = str(os.getcwd()) + "/"
        self.exe_file = sys.argv[0]
        self.parser = argparse.ArgumentParser(
            description=self.descripcion,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
        self.debug = debug
        # fichero de log y su limite de lineas por defecto
        self.logfile = str(self.ruta + self.nombre.replace(" ", "_") + ".log")
        self.limit_log = 5
        # si se excede el num de lineas borra desde la 0 hasta el limite
        if cuenta_lineas(self.logfile) > self.limit_log:
            borrar_lineas(self.logfile, 0, self.limit_log)
        self.cabecera()

    # @@@@@@@@@@@@@@@@@@@ METODOS DE REPORTE E INFO @@@@@@@@@@@@@@@@@@@@@@@
    # imprime la cabecera
    def cabecera(self):
        print "\n"
        print formato_str("amarillo", "  # Script: ") + self.nombre
        print formato_str("amarillo", "  # Descripción: ") + self.descripcion
        print formato_str("amarillo", "  # Enlace: ") + self.enlace
        print "\n"

    # escribe el mensaje en el fichero de log
    def log(self, mensaje):
        texto = fecha() + hora() + " " + mensaje + "\n"
        f = open(self.logfile, "a")
        f.write(str(texto))
        f.close()

    # escribe mensaje informativo en la consola
    def msg(self, mensaje):
        if self.debug:
            print formato_str("azul", "\t[i] ") + mensaje
        # TODO: guardar log

    # escribe mensaje de error en la consola
    def err(self, mensaje):
        if self.debug:
            print formato_str("rojo", "\t[!] ") + mensaje
        # TODO: guardar log

    # muestra la ayuda
    def ayuda(self):
        self.parser.print_help()

    # @@@@@@@@@@@@@@@@@ METODOS DE GESTION PARAMETROS @@@@@@@@@@@@@@@@@@@@@
    # añade opcion boolean
    def add_op_boolean(self, opcion, mensaje, defecto=False):
        if defecto:
            accion = 'store_false'
        else:
            accion = 'store_true'
        self.parser.add_argument(
            opcion, help=mensaje, action=accion, default=defecto
        )

    # añade lista de opciones
    def add_op_lista(self, opcion, lista, mensaje):
        self.parser.add_argument(opcion, choices=lista, help=mensaje)

    # añade opcion que requiere un valor/parametro
    def add_op_valor(self, opcion, mensaje, tipo=str):
        self.parser.add_argument(opcion, help=mensaje, type=tipo)

    # parsea los argumentos
    def check_args(self):
        self.args = self.parser.parse_args()
        return self.args

    # @@@@@@@@@@@@@@@@@@@    O T R O S   M E T O D O S @@@@@@@@@@@@@@@@@@@@@
    # crea una lista de opciónes y lo comprueba antes de devolver el resultado
    def lista_menu(self, lista, mensaje):
        while True:
            # limpia y añade la opcion 0 si no existe
            self.limpia_pantalla()
            print "\t" + mensaje
            if lista[0] != "Salir del menu":
                lista.insert(0, "Salir del menu")
            # imprime la lista en forma de menu
            for i in range(1, len(lista)):
                print "\n\t\t" + str(i) + " - " + lista[i]
            print "\n\n\t\t" + "0 - " + lista[0]
            # pide un valor al usuario
            try:
                opcion = int(raw_input("\n\n\tSelecciona una opción: "))
            # si no es un int repite el bucle
            except ValueError:
                pass
            # si es tipo int
            else:
                # si es un indice existente devuelve el valor y rompe el bucle
                if len(lista) > opcion:
                    return opcion
                    break

    # función que limpia la consola e imprime cabecera de nuevo
    def limpia_pantalla(self):
        if sys.platform == "linux2":
            os.system('clear')
        else:
            os.system('cls')
        self.cabecera()


# FUNCIONES
def cuenta_atras(texto, segundos):
    timer = segundos
    while timer > 0:
        print texto + str(math.ceil(timer)) + " sec]",
        print "\r",
        time.sleep(0.01)
        timer -= 0.01

# funcion que devuelve un string con el formato especificado,
def formato_str(f, texto):
    if f == "rosa":
        return '\033[95m' + texto + '\033[0m'
    elif f == "azul":
        return '\033[94m' + texto + '\033[0m'
    elif f == "verde":
        return '\033[92m' + texto + '\033[0m'
    elif f == "amarillo":
        return '\033[93m' + texto + '\033[0m'
    elif f == "rojo":
        return '\033[91m' + texto + '\033[0m'
    elif f == "subraya":
        return '\033[4m' + texto + '\033[0m'
    elif f == "negrita":
        return '\033[1m' + texto + '\033[0m'
    else:
        return texto


# devuelve fecha
def fecha():
    return str(time.strftime("[%d/%m/%y]"))


# devuelve la hora
def hora():
    return str(time.strftime("[%X]"))


# devuelve el numero lineas de fichero
def cuenta_lineas(fichero):
    try:
        f = open(fichero, 'r')
        num_lineas = len(f.readlines())
        f.close()
    except:
        return 0
    return int(num_lineas)


# borra las lineas especificadas de un fichero, de X a Y
def borrar_lineas(fichero, primera, ultima):
    # abre el fichero y lo guarda todo en un array
    f = open(fichero, "r")
    nuevo = []
    for linea in f.readlines():
        nuevo.append(linea)
    # cierra el fichero y lo reabre en modo escritura
    f.close
    f = open(fichero, "w")
    # escribe las lineas del array que NO estén entre 'primero' y 'ultima'
    for i in range(0, len(nuevo)):
        if i >= primera and i <= ultima:
            pass
        else:
            f.write(nuevo[i])
    f.close()

# TODO: hacer funcion para html: https://www.decalage.info/python/html


if __name__ == "__main__":
    print "\n\tEste modulo no puede ser ejecutado como script."
    print "\tVer documentación y ejemplos en README.md\n"
else:
    pass
